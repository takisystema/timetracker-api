## RESTful URLs

### Timetracker
* Update user timetracker by id:
    * PUT https://desolate-brook-41820.herokuapp.com/api/time/:id
    request body:
    ```json
    {
        "name": "<string>",
        "instances":{
            "startDate": 1556994415601,
    	    "endDate": 1556994015601,
    	    "tags": ["string"]
        }
    }
    ```
* Delete user timetracker by id:
    * DELETE https://desolate-brook-41820.herokuapp.com/api/time/:id
    response body:
    ```json
    {   
        "todoList": [{
            "name": "<todoList.name>",
            "_id": "<todoList.id>",
            "instances": {
                "startDate": 1556994415601,
                "endDate": 1556994015601,
                "tags": ["string"]
            }
        }]
    }
    ```
* Get user timetracker by id:
    * GET https://desolate-brook-41820.herokuapp.com/api/time/:id
    response body:
    ```json
    {   
        "name": "<name>",
        "_id": "<id>",
        "instances": {
            "startDate": 1556994415601,
            "endDate": 1556994015601,
            "tags": ["string"]
        }
    }

### Users
* Register user:
    * POST https://desolate-brook-41820.herokuapp.com/api/user/register
    request body:
    ```json
    {
        "username": "<username>",
        "email": "<email>",
        "password": "<password>",
        "active": "true",
    }
    ```
* Login user:
    * POST https://desolate-brook-41820.herokuapp.com/api/user/login
    request body
    ```json
    {
        "email": "<email>",
        "password": "<password>"
    }
    ```
* Logout user:
    * PUT https://desolate-brook-41820.herokuapp.com/api/user/logout
    header:
    jwt token
* Get user by id:
    * Get https://desolate-brook-41820.herokuapp.com/api/user/:id
    response body:
    ```json
    {   
        "id": 0,
        "username": "<username>",
        "email": "<email>",
        "active": "true",
        "todoList": [{
            "name": "<todoList.name>",
            "_id": "<todoList.id>",
            "instances": {
                "startDate": 1556994415601,
                "endDate": 1556994015601,
                "tags": ["string"]
            }
        }]
    }
    ```