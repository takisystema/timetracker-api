const mongoose = require("mongoose");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Users = require("../models/user");
const { isEmail } = require("../common/common");
const verifyToken = require("../middleware/verifyToken");

const router = express.Router();
const ObjectId = mongoose.Types.ObjectId;

/*
 **
 **    Users routes
 **
 **
 */

// Registering user
router.post("/user/register", async (req, res) => {
  try {
    const { username, email, password, active } = req.body;
    if (!isEmail(email)) {
      throw new Error("Email must be a valid email address.");
    }
    if (typeof password !== "string") {
      throw new Error("Password must be a string.");
    }
    const user = new Users({ username, email, password, active });
    const persistedUser = await user.save();

    res.status(201).json({
      title: "User Registration Successful",
      detail: "Successfully registered new user",
    });
  } catch (err) {
    res.status(400).json({
      errors: [
        {
          title: "Registration Error",
          detail: "Something went wrong during registration process.",
          errorMessage: err.message,
        },
      ],
    });
  }
});

// Login
router.post("/user/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!isEmail(email)) {
      return res.status(400).json({
        errors: [
          {
            title: "Bad Request",
            detail: "Email must be a valid email address",
          },
        ],
      });
    }
    if (typeof password !== "string") {
      return res.status(400).json({
        errors: [
          {
            title: "Bad Request",
            detail: "Password must be a string",
          },
        ],
      });
    }
    //queries database to find a user with the received email
    const user = await Users.findOne({ email });
    if (!user) {
      throw new Error();
    }

    //using bcrypt to compare passwords
    const passwordValidated = await bcrypt.compare(password, user.password);
    if (!passwordValidated) {
      throw new Error();
    }

    // Mask user pwd and other infos.
    const maskedUser = {
      id: user._id,
      username: user.username,
      email: user.email,
    };

    // JSON Web Token
    jwt.sign({ user: maskedUser }, "timekey", (err, token) => {
      Users.findByIdAndUpdate(
        { _id: maskedUser.id },
        { $set: { sessionToken: token } },
        { new: true }
      ).then(() => {
        Users.findOne({ _id: maskedUser.id }).then(() => {
          Users.aggregate([
            {
              $project: {
                password: 0,
              },
            },
          ]).then(() => {
            res.json({
              title: "Login Successful",
              detail: "Successfully validated user credentials",
              sessionToken: token,
              userId: maskedUser.id,
            });
          });
          // TODO: Find out if it's save to show userId
        });
      });
    });
  } catch (err) {
    res.status(401).json({
      errors: [
        {
          title: "Invalid Credentials",
          detail: "Check email and password combination",
          errorMessage: err.message,
        },
      ],
    });
  }
});

// Logout
router.put("/user/logout", verifyToken, (req, res, next) => {
  jwt.verify(req.token, "timekey", (err, authData) => {
    if (err) {
      res.status(403).json({ error_message: err, token: req.token });
    } else {
      Users.findByIdAndUpdate(
        { _id: authData.user.id },
        { sessionToken: null },
        { new: true }
      )
        .then(() => {
          Users.findById({ _id: authData.user.id }).then(() => {
            // Hide psw
            Users.aggregate([
              {
                $project: {
                  password: 0,
                },
              },
            ]).then((timetracker) => {
              res.send(timetracker);
            });
          });
        })
        .catch(next);
    }
  });
});

// get user by id
router.get("/user/:id", (req, res, next) => {
  console.log("res", res);
  // res.sendStatus(403);
  Users.findOne({ _id: req.params.id })
    .then((times) => {
      // Add total to days into instances
      Users.aggregate([
        //find object id
        {
          $match: { _id: ObjectId(req.params.id) },
        },
        // extract timetracker and instances
        {
          $unwind: {
            path: "$timetrackers",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $unwind: {
            path: "$timetrackers.instances",
            preserveNullAndEmptyArrays: true,
          },
        },
        // add total into timetrackers
        {
          $addFields: {
            "timetrackers.instances.total": {
              $subtract: [
                "$timetrackers.instances.endDate",
                "$timetrackers.instances.startDate",
              ],
            },
          },
        },
        // group up to reconstruct unwinded array
        {
          $group: {
            _id: "$_id",
            username: { $first: "$username" },
            email: { $first: "$email" },
            active: { $first: "$active" },
            timetrackers: {
              $push: "$timetrackers",
            },
          },
        },
      ]).then((times) => {
        res.send(times);
      });
    })
    .catch(next);
});

// update a user in the db
router.put("/user/:id", verifyToken, (req, res, next) => {
  jwt.verify(req.token, "timekey", (err, authData) => {
    if (err) {
      res.status(403).send(err);
    } else {
      Users.findById({ _id: authData.user.id })
        .then(() => {
          Users.findByIdAndUpdate({ _id: req.params.id }, req.body).then(() => {
            // Hide psw
            Users.aggregate([
              {
                $project: {
                  password: 0,
                  sessionToken: 0,
                },
              },
            ]).then((timetracker) => {
              res.send(timetracker);
            });
          });
        })
        .catch(next);
    }
  });
});

module.exports = router;
