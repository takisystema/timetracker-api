const mongoose = require('mongoose');
const express = require ('express');
const jwt = require('jsonwebtoken');
const Users = require('../models/user');
const verifyToken = require('../middleware/verifyToken');

const router = express.Router();
const ObjectId = mongoose.Types.ObjectId;

// TODO: Refresh token
// TODO: Calculate total_instances

/*
**
**    Timetrackers routes
**
**
*/

// Create timetracker Object
router.post('/time', verifyToken, (req, res, next) => {
  jwt.verify(req.token, 'timekey', (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      Users.findById({_id: authData.user.id}).then((user) => {
        const timetrackerObj = req.body;

        if (!user) throw new Error('no user with that id');

        user.timetrackers.push(timetrackerObj);
        user.save((err, data) => {
          if (err) {
              res.statusCode = 500;
              return res.json({ title: 'Save Error', message: err });
          }
          console.log('Success!');
          res.send(data.timetrackers)
        });
      }).catch(next);
    }
  })
});

// update timetracker by id
router.put('/time/:id', verifyToken, (req, res, next) => {
  
  // Get only the keys that needs to be updated
  let setKey = {};
  Object.keys(req.body).forEach(key => {
    setKey[`timetrackers.$.${key}`] = req.body[key]; 
  });

  jwt.verify(req.token, 'timekey', (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      Users.findById({'_id': authData.user.id}).then(() => {
        Users.findOneAndUpdate(
          {'_id': authData.user.id , 'timetrackers._id': req.params.id },
          {$set: setKey},
          {new: true}
        ).then(() => {
          // Hide psw
          Users.aggregate([
          {
              $project: {
                  password: 0,
                  sessionToken: 0
              }
          }
          ]).then(function(tracker){
              res.send(tracker)
          });
        });
      }).catch(next);
    }
  })
});


// delete timetracker by id
router.delete('/time/:timetrackerId', verifyToken, (req, res, next) => {
  jwt.verify(req.token, 'timekey', (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      Users.findById({_id: authData.user.id}).then(() => {
        Users.findByIdAndRemove({'timetrackers._id': req.params.timetrackerId}).then((timetracker) => {
          res.send(timetracker);
        });
      }).catch(next);
    }
  })
});

// Get timetracker by id
router.get('/time/:id', verifyToken, (req, res, next) => {
    jwt.verify(req.token, 'timekey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
          Users.findById({_id: authData.user.id}).then(() => {
            Users.findOne({'timetrackers._id': req.params.id }, { 'timetrackers.$': 1 }).then((timetracker) => {
              res.send(timetracker.timetrackers[0]);
            });
          }).catch(next);
        }
    });
});


/*
**
**    Timetracker instances routes
**
**
*/

// Add New Instance to a specific timetracker
router.post('/time/:timetrackerId/instance', verifyToken, (req, res, next) => {
  jwt.verify(req.token, 'timekey', (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      Users.findById({_id: authData.user.id}).then(() => {
        Users.findOneAndUpdate(
          { 'timetrackers._id': req.params.timetrackerId },
          { $push: { 'timetrackers.$.instances': req.body } },
          { new: true}
        ).then((instance) => {
          res.send(instance.timetrackers);
        });
      }).catch(next);
    }
  })
});

// Delete instance by id
router.delete('/time/:timetrackerId/instance/:instanceId', verifyToken, (req, res, next) => {
  jwt.verify(req.token, 'timekey', (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      Users.findById({_id: authData.user.id}).then(() => {
        Users.findOneAndUpdate(
          { 'timetrackers._id': req.params.timetrackerId },
          { 
            $pull: { 
              'timetrackers.$.instances': {'_id': ObjectId(req.params.instanceId)}
            }
          },
          { new: true }, (err, data) => {
          if (err) {
              res.statusCode = 500;
              return res.json({ title: 'Deleting Error', message: err });
          }
          res.send(data.timetrackers)
        });
      }).catch(next);
    }
  })
});

// Edit a Timetracker Instance
router.put('/time/:timetrackerId/instance/:instanceId', verifyToken, (req, res, next) => {
  jwt.verify(req.token, 'timekey', (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      Users.findById({_id: authData.user.id}).then((user) => {
        const timetracker = user.timetrackers;
        const trackerIndex = timetracker.findIndex( tracker => tracker._id == req.params.timetrackerId );
        let set = {$set: {}};
        set.$set[`timetrackers.${trackerIndex}.instances.$`] = req.body
        
        Users.findOneAndUpdate(
          {
            'timetrackers._id': req.params.timetrackerId,
            'timetrackers.instances._id': req.params.instanceId
          },
          set,
          { new: true }, (err, data) => {
          if (err) {
              res.statusCode = 500;
              return res.json({ title: 'Deleting Error', message: err });
          }
          res.send(data.timetrackers)
        });
      }).catch(next);
    }
  })
});


module.exports = router;
