const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const mongoose = require('mongoose');

// set up express app
const app = express();
const options = {
	useNewUrlParser: true
}

// CORS
app.use(cors());

// Global promise
mongoose.Promise = global.Promise;

// connect to mongodb
mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost:27017/timetracker', options).then(function() {
// mongoose.connect('mongodb://admin:88PNuue6GR8zikM@ds127995.mlab.com:27995/timetracker', options).then(function() {
	console.log('Connected to mongoDB');
}, function(err) {
	console.log('Error connecting to mongoDB', err)
});

// ensure a unique value without getting deprecation warning
mongoose.set('useCreateIndex', true);

//set up static files
app.use(express.static('public'));

// use body-parser middleware
app.use(bodyParser.json());

// use cookie-parser middleware
app.use(cookieParser());

// initialize routes
app.use('/api', require('./routes/timetrackerRoutes'));
app.use('/api', require('./routes/userRoutes'));

// error handling middleware
app.use(function(err, req, res, next){
    console.log(err); // to see properties of message in our console
    res.status(422).send({error: err.message});
});

// listen for requests
const server = app.listen(process.env.PORT || 4000, function(){
    const port = server.address().port;
    console.log(`now listening for requests ${port}`);
});
