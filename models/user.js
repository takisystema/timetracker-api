const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
const TimetrackerSchema = require('./timetracker');

const Schema = mongoose.Schema;

// Create User Schema & model
const UserSchema = new Schema({
    username: {
        type: String,
        lowercase: true,
        unique: true,
        required: [true, 'Username field is required'],
        index: true
    },
    email: {
        type: String,
        minlength: 1,
        trim: true,
        unique: true,
        required: [true, 'Username field is required'],
        index: true
    },
    password: {
        type: String,
        minlength: 8,
        required: [true, 'Password field is required']
    },
    active: {
        type: Boolean,
        default: false
    },
    sessionToken: {
        type: String,
        default: false
    },
    timetrackers: [TimetrackerSchema]
});

UserSchema.plugin(uniqueValidator);

UserSchema.pre('save', function(next) {
  let user = this;

  if (!user.isModified('password')) {
    return next();
  }

  //we generate the salt using 12 rounds and then use that salt with the received password string to generate our hash
  bcrypt
    .genSalt(12)
    .then((salt) => {
      return bcrypt.hash(user.password, salt);
    })
    .then((hash) => {
      user.password = hash;
      next();
    })
    .catch((err) => next(err));
});

const User = mongoose.model('user', UserSchema);

module.exports = User;
