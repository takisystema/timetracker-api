const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Instance = new Schema({
    startDate: {
        type: Date,
        default: Date.now,
        required: [true, 'Start date field is required']
    },
    endDate:{
        type: Date,
        default: () => Date.now() + 7*24*60*60*1000,
        required: [false]
    },
    total: {
        type: Date, // endDate - startDate
        required: false
    }
})

// Create Timetracker Schema & model
const TimetrackerSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Timetracker name is required']
    },
    instances: [Instance],
    total_instances: {
        type: Date, // endDate - startDate
        required: false
    },
    tags: [{
        type: String,
        required: false
    }]
});


module.exports = TimetrackerSchema;
